﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codility_Console.Chapters
{
    public class Array_Shift
    {
        public int[] Solution(int[] arr,int k)
        {
            if (arr == null || arr.Length == 0)
                return arr;
            if (k == 0)
                return arr;
            int step = k;
            if (k == arr.Length)
                return arr;
            else if (k > arr.Length)
                step = k % arr.Length;
            int[] temp = new int[arr.Length];
            int j = step;
            for(int i = 0; i < arr.Length; i++)
            {
                temp[j] = arr[i];
                j++;
                if (j == arr.Length)
                    j = 0;
            }
            return temp;
        }
    }
}
