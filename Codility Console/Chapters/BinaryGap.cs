﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codility_Console.Chapters
{
    public class Iteration_BinaryGap
    {
        public int Solution(int N)
        {
            //string bN = Convert.ToString(N, 2);
            //int topLen = 0;
            //int index = 0;
            //do
            //{
            //    index = bN.IndexOf('1');
            //    if (index == -1)
            //        break;
            //    bN = bN.Substring(index + 1);
            //    var count = Count(bN);
            //    if (count == -1)
            //        break;
            //    topLen = count > topLen ? count : topLen;
            //}
            //while (index != -1);
            //return topLen;

            string bN = Convert.ToString(N, 2);
            bool start = false;
            int count = 0, total = 0;
            foreach (char ch in bN)
            {
                if (ch == '1')
                {
                    start = true;
                    if (count > total)
                        total = count;
                    count = 0;
                }
                if (start)
                    count++;
            }
            return total;
        }

        private int Count(string tbN)
        {
            return tbN.IndexOf('1');
        }

    }
}
