using Codility_Console.Chapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Codility_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            for(; ; )
            {
                Console.WriteLine("\r\n1: Enter Value");
                Console.WriteLine("2: Exit");
                int choice = int.Parse(Console.ReadLine());
                if (choice == 2)
                    break;
                Execute();
            }
        }

        static void Execute()
        {
            Console.WriteLine("Enter array size:");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter array:");
            int[] arr = new int[n];
            for (int i = 0; i < n; i++)
                arr[i] = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter k:");
            int k = int.Parse(Console.ReadLine());
            Array_Shift array = new Array_Shift();
            var temp=array.Solution(arr, k);
            foreach (var item in temp)
                Console.Write(item + ", ");

        }

    }
}
